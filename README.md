**Please do not commit anything to master!  Create your own branch and push that!**

This is designed to run in Linux.  It has been tested in CentOS and Debian distros

This requires Java 8 to be your default JRE.  Execute ```java -version``` to make sure.

If it is not, Ubuntu/Mint uses the /etc/environment file for global environment variables,
so you can sudo vi a change to it to set the JAVA_HOME path.  It should be something like /usr/lib/jvm/java-8-oracle/

Instructions for creating a basic widget are here:

https://github.com/ozoneplatform/owf-framework/wiki/OWF-7-Developer-Creating-a-Widget

To clarify the instructions...

Start the server by running ```startServer.sh```

You should create a folder outside of the OWF project that contains your widget.  The folder name needs to be what you want to call your widget in the URL path

In that folder, there should be a WEB-INF folder

In the WEB-INF, create a web.xml file with something like this:

```<?xml version="1.0" encoding="UTF-8"?>
<Web-app version="2.4" xmlns="http://java.sun.com/xml/ns/j2ee"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee 
    http://java.sun.com/xml/ns/j2ee/Web-app_2_4.xsd">
    <display-name>Simple Announcing Clock Widget</display-name>
</Web-app>```

**Protip:  When creating an XML document, the very first character must be the '<' symbol in the '<?xml version...' string, so be careful when copying and pasting**


In the root of your widget folder, create an HTML file with the contents of your widget.

Once finished, create the war file:   ```jar cvf announcing-clock.war .```
_Note the period at the end of the command_

Copy the war file to the staging/apache-tomcat/webapps folder.  It should automagically be deployed, but keep an eye on the server output for any deployment errors.